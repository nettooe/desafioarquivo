package br.com.oliver.desafioarquivo.test;

import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ibatis.common.jdbc.ScriptRunner;

import br.com.oliver.desafioarquivo.DatabaseConnection;
import br.com.oliver.desafioarquivo.Main;

public class MainTest {

	private static String pastaDestino;

	@BeforeClass
	public static void preparar() {
		prepararBancoDeDados();

		lerArquivoDePropriedades();
	}

	/**
	 * Executa teste em caminho feliz.
	 */
	@Test
	public void testExecutar() {

		// esta data existe na massa de testes.
		String dataGerar = "31082018";
		String[] args = { dataGerar };

		Main main = new Main();
		main.executar(args);

		boolean arquivoExiste = new File(pastaDestino, dataGerar + ".txt").exists();
		assertTrue(arquivoExiste);

	}

	/**
	 * Executa teste para data que não possui transações.
	 */
	@Test
	public void testExecutar_dataSemTransacoes() {

		// esta data NÃO existe na massa de testes.
		String dataGerar = "31082020";
		String[] args = { dataGerar };

		Main main = new Main();
		main.executar(args);

		boolean arquivoExiste = new File(pastaDestino, dataGerar + ".txt").exists();
		assertTrue(arquivoExiste);
	}

	/**
	 * Executa teste para data inválida no calendário.
	 */
	@Test
	public void testExecutar_dataInvalida() {
		try {
			// esta data NÃO existe na massa de testes.
			String dataGerar = "99082020";
			String[] args = { dataGerar };

			Main main = new Main();
			main.executar(args);
		} catch (Exception e) {
			assertTrue(e.getMessage().contains("Data informada é inválida"));
		}

	}

	/**
	 * Testar caso seja não seja informada uma data
	 */
	@Test
	public void testExecutar_argumentoNaoInformado() {
		try {
			String dataGerar = new String();
			String[] args = { dataGerar };

			Main main = new Main();
			main.executar(args);
		} catch (Exception e) {
			assertTrue(e.getMessage().contains("Obrigatório informar data das transações"));
		}

	}

	/**
	 * testar caso seja informado null como argumento.
	 */
	@Test
	public void testExecutar_argumentoNulo() {
		try {
			Main main = new Main();
			main.executar(null);
		} catch (Exception e) {
			assertTrue(e.getMessage().contains("Obrigatório informar data das transações"));
		}

	}

	private File obterArquivoDeResources(String nomeArquivo) {
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource(nomeArquivo).getFile());
		return file;
	}

	private static void lerArquivoDePropriedades() {
		InputStream streamPropriedades = null;
		try {
			streamPropriedades = Main.class.getClassLoader().getResourceAsStream("config.properties");

			Properties propriedades = new Properties();
			propriedades.load(streamPropriedades);

			pastaDestino = propriedades.getProperty("pastaDestino");

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	/**
	 * Prepara o banco de dados de testes com os scripts da pasta resources
	 * 
	 * @throws SQLException
	 * @throws IOException
	 */
	private static void prepararBancoDeDados() {

		Connection con = null;
		try {
			con = DatabaseConnection.getConnection();

			// Inicializa o objeto ScriptRunner
			ScriptRunner sr = new ScriptRunner(con, false, false);

			// obtem o script de DDL a partir da pasta test/resources
			Reader reader = new BufferedReader(
					new FileReader(new MainTest().obterArquivoDeResources("01_ddl_create_tabela_transacao.sql")));

			// Exctute script
			sr.setDelimiter(";", false);
			sr.runScript(reader);

			// obtem o script de DDL a partir da pasta test/resources
			Reader readerProcedure = new BufferedReader(
					new FileReader(new MainTest().obterArquivoDeResources("02_ddl_create_procedures.sql")));

			// Executa script
			sr.setDelimiter("$$", false);
			sr.runScript(readerProcedure);

			Reader massa = new BufferedReader(
					new FileReader(new MainTest().obterArquivoDeResources("03_massa_de_testes.sql")));
			sr.setDelimiter(";", false);
			sr.runScript(massa);

		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (con != null && !con.isClosed()) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}

	}
}
