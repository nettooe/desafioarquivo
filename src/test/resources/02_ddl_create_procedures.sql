DROP PROCEDURE IF EXISTS obterTransacoesPorData$$
 
CREATE PROCEDURE obterTransacoesPorData(IN param_data DATE)
BEGIN
    SELECT CONCAT(t.cartao, REPLACE(LPAD(t.valor, 12, 0), '.', ''), DATE_FORMAT(t.data, "%d%m%Y")) transacao
    FROM transacao t
    where t.data = param_data;
END $$