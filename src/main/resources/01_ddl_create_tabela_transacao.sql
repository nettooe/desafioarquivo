CREATE TABLE `transacao` (
  `idTransacao` bigint(20) NOT NULL AUTO_INCREMENT,
  `cartao` bigint(16) NOT NULL,
  `valor` decimal(11,2) NOT NULL,
  `data` date NOT NULL,
  PRIMARY KEY (`idTransacao`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;