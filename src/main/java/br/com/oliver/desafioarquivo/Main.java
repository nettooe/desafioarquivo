package br.com.oliver.desafioarquivo;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

public class Main {

	private String pastaDestino;

	public static void main(String[] args) {

		new Main().executar(args);

	}

	public void executar(String[] args) {

		// valida se o arquivo de propriedades contém todos os parâmetros de
		// configuração necessários
		validarArquivoDePropriedades();

		long inicioPrograma = System.currentTimeMillis();

		Connection conexao = null;
		try {
			// realiza a leitura do argumento passado na execução do programa
			Date dataTransacao = obterArgumentoDataTransacao(args);

			// obtém a conexão com o banco de dados
			conexao = DatabaseConnection.getConnection();

			// Prepara a chamada à procedure
			PreparedStatement stmt = conexao.prepareStatement("call obterTransacoesPorData(?);");

			// a data informada como argumento é passada para a procedure
			stmt.setString(1, new SimpleDateFormat("yyyy-MM-dd").format(dataTransacao));

			// realiza a chamada à procedure
			ResultSet resultSet = stmt.executeQuery();
			
			new File(pastaDestino).mkdirs();

			String arquivoDestino = pastaDestino + "/" + new SimpleDateFormat("ddMMyyyy").format(dataTransacao)
					+ ".txt";

			// prepara um arquivo vazio que receberá o resultado da consulta
			BufferedWriter bw = new BufferedWriter(new FileWriter(arquivoDestino, false));

			// percorre todo o retorno da consulta
			while (resultSet.next()) {
				// escreve no arquivo cada linha retornada na consulta
				bw.write(resultSet.getString("transacao"));
				bw.newLine();
			}

			// fecha o arquivo gerado
			bw.close();

			System.out.println("arquivo gerado: " + arquivoDestino);

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (conexao != null && !conexao.isClosed()) {
					conexao.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}

		// informa o término
		System.out.println("Fim do programa em: " + (System.currentTimeMillis() - inicioPrograma) + "ms");
	}

	private Date obterArgumentoDataTransacao(String[] args) throws Exception {
		if (args == null || args.length == 0) {
			String mensagem = "Obrigatório informar data das transações. Exemplo para 31/08/2018: java Main 31082018";
			System.err.println(mensagem);
			throw new Exception(mensagem);
		}

		String argumento = args[0];

		if (argumento.length() != 8) {
			String mensagem = "Obrigatorio informar data das transações com o formato especificado. Exemplo para 31/08/2018: java Main 31082018";
			System.err.println(mensagem);
			throw new Exception(mensagem);
		}

		Date data = null;
		SimpleDateFormat formatoEntrada = new SimpleDateFormat("ddMMyyyy");
		try {
			formatoEntrada.setLenient(false);
			data = formatoEntrada.parse(argumento);
		} catch (ParseException e) {
			String mensagem = "Data informada é inválida. Obrigatorio informar data das transações com o formato especificado. Exemplo para 31/08/2018: java Main 31082018";
			System.err.println(mensagem);
			throw new Exception(mensagem);
		}
		return data;
	}

	/**
	 * Valida se o arquivo de propriedades contém todos os parâmetros de
	 * configuração necessários
	 */
	private void validarArquivoDePropriedades() {
		InputStream streamPropriedades = null;
		try {
			streamPropriedades = Main.class.getClassLoader().getResourceAsStream("config.properties");

			Properties propriedades = new Properties();
			propriedades.load(streamPropriedades);

			String host = propriedades.getProperty("host");
			String porta = propriedades.getProperty("porta");
			String nomeBancoDeDados = propriedades.getProperty("nomeBancoDeDados");
			String usuario = propriedades.getProperty("usuario");
			String senha = propriedades.getProperty("senha");
			String useSSL = propriedades.getProperty("useSSL");

			pastaDestino = propriedades.getProperty("pastaDestino");

			Set<String> parametrosFaltantes = new HashSet<String>();

			if (host == null) {
				parametrosFaltantes.add("host");
			}
			if (porta == null) {
				parametrosFaltantes.add("porta");
			}
			if (nomeBancoDeDados == null) {
				parametrosFaltantes.add("nomeBancoDeDados");
			}
			if (usuario == null) {
				parametrosFaltantes.add("usuario");
			}
			if (senha == null) {
				parametrosFaltantes.add("senha");
			}
			if (useSSL == null) {
				parametrosFaltantes.add("useSSL");
			}
			if (pastaDestino == null) {
				parametrosFaltantes.add("pastaDestino");
			}

			if (!parametrosFaltantes.isEmpty()) {
				StringBuilder message = new StringBuilder(
						"Não foram encontrados os seguintes parametros obrigatorios no arquivo de propriedades:");

				for (String param : parametrosFaltantes) {
					message.append("\n");
					message.append(param);
				}

				System.err.println(message.toString());
				System.exit(0);
			}

		} catch (IOException e) {
			e.printStackTrace();

			if (streamPropriedades != null) {
				try {
					streamPropriedades.close();
				} catch (IOException ie) {
					ie.printStackTrace();
				}
			}
		}

	}

}
