package br.com.oliver.desafioarquivo;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DatabaseConnection {

	private static Connection connection;

	/**
	 * obtém a conexão com o banco de dados
	 * @return conexao
	 * @throws IOException 
	 * @throws SQLException 
	 */
	public static Connection getConnection() throws IOException, SQLException {
		if (connection == null || connection.isClosed()) {
			InputStream streamPropriedades = null;
			try {
				streamPropriedades = Main.class.getClassLoader().getResourceAsStream("config.properties");

				// load a properties file
				Properties propriedades = new Properties();
				propriedades.load(streamPropriedades);

				String host = propriedades.getProperty("host");
				String porta = propriedades.getProperty("porta");
				String nomeBancoDeDados = propriedades.getProperty("nomeBancoDeDados");
				String usuario = propriedades.getProperty("usuario");
				String senha = propriedades.getProperty("senha");
				String useSSL = propriedades.getProperty("useSSL");

				String connectionString = "jdbc:mysql://" + host + ":" + porta + "/" + nomeBancoDeDados + "?useSSL="
						+ useSSL;

				connection = DriverManager.getConnection(connectionString, usuario, senha);

				return connection;
			} catch (IOException e) {
				System.out.println("Houve um erro ao ler o arquivo de propriedades.");
				e.printStackTrace();
				throw e;
			} catch (SQLException e) {
				System.err.println("Houve um erro ao conectar com o Banco de Dados.");
				e.printStackTrace();
				throw e;
			} finally {
				if (streamPropriedades != null) {
					try {
						streamPropriedades.close();
					} catch (IOException ie) {
						ie.printStackTrace();
					}
				}

			}
		}

		return connection;
	}

}
