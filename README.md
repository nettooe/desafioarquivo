**O que esta aplicação faz**

"Geração de um arquivo com as transações de acordo com a data informada como parametro na execução da aplicação."

Desenvolvido por: Óliver Emanuel <netto.oe@gmail.com>


---

## Como baixar a solução e executar

Esta aplicação utiliza banco de dados MySQL para demonstrar a implementação da solução.

### 1. Clone o repositório

Abra o terminal e digite:

$ git clone https://nettooe@bitbucket.org/nettooe/desafioarquivo.git

Em seguida vá até a pasta desafioarquivo que foi criada.

---

### 2.Configure o arquivo de propriedades

Abra o arquivo src/main/resources/config.properties e ajuste as propriedades para conectar com o banco de dados MySQL:


host : é o IP ou nome da máquina que contém o servidor de banco de dados MySQL

porta : é a porta de acesso ao banco de dados

nomeBancoDeDados : é o nome para o banco de dados (previamente existente)

useSSL : caso utilize SSL para comunicar com o banco de dados, utilize true, ou mantenha false

usuario : usuário com permissão para consulta no banco de dados

senha : senha do usuário

pastaDestino : é o caminho onde serão gravados os arquivos de saída para cada data consultada. Exemplo: /home/oliver/desafioarquivo


*Os testes unitário também utilizam banco de dados MySQL. O arquivo de configurações fica no caminho: src/test/resources/config.properties
*O usuário para o banco de dados de testes deverá ter permissão para criação de tabelas e procedures.
**CUIDADO** para não utilizar as mesmas informações no arquivo de produção e no de testes!

---

### 3.Compile a aplicação

Depois de realizar os passos anteriores, você pode querer usar sua IDE preferida para compilar este projeto Maven ou ainda utilizar o MavenWrapper que adicionei no repositório.
Para compilar, utilize um terminal (ou prompt de commando) e, estando na pasta do projeto, digite:

$ ./mvnw clean install

### 4.Execute os scripts do banco de dados*

*Este passo não é necessário para o banco de dados de testes. É necessário apenas para o banco de dados produtivo.

Conecte no banco de dados MySQL que você indicou no arquivo de configurações (src/main/resources/config.properties) e execute
os scripts

src/main/resources/*01_ddl_create_tabela_transacao.sql*
src/main/resources/*02_ddl_create_procedures.sql*

Estes scripts criação uma tabela e uma Stored Procedure no seu banco de dados que será utilizada pela aplicação.

### 5.Execute a aplicação

Agora, para executar a aplicação, você deve informar a data das transações que a aplicação irá obter do banco de dados e irá gerar o arquivo.
no exemplo abaixo -Dexec.args="31082018" indica que se deseja gerar um arquivo com todas as transações do dia 30/agosto/2018.

$ ./mvnw exec:java -Dexec.mainClass="br.com.oliver.desafioarquivo.Main" -Dexec.cleanupDaemonThreads=false -Dexec.args="31082018"

## Diferenciais

Esta implemementação do desafio tem os seguintes diferencias:

1. Utiliza chamada a Stored Procedure: melhor tempo de resposta do banco de dados, visto que o plano de execução já está está definido pelo MySQL desde o momento da implantação da procedure;
2. Não utiliza frameworks: proporciona menor uso de recursos de CPU e memória RAM, pois não é necessário inicializar mais bibliotecas desnecessárias;
3. Formatação da linha do arquivo de saída feita na procedure: desta forma não é necessário tratar linha-a-linha dentro do Java;

## Sugestões de melhorias

1. Paginação: se a quantidade de registros for superior a 1 milhão retornado pelo banco de dados, pode ser conveniente dividir em threads as chamdas à procedure trazendo os dados de forma paginada e realizar a escrita do arquivo;
2. Disponibilizar a aplicação como serviço: se este core for adicionado dentro de uma aplicação utilizando SpringBoot, sendo disponibilizado através de um serviço REST, o tempo de resposta poderá ser menor, pois a conexão com o banco de dados já estará pré-estabelecida;
3. Construção de índice para o campo "data" na tabela: a construção do índice poderá trazer maior performance às consultas realizadas pela procedure;